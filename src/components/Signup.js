import React from "react";
import "../components/Signup.css";
import Background from "../img/Background.png";
import { GoMail } from "react-icons/go";
import { AiOutlineLock } from "react-icons/ai";
import { AiOutlineUser } from "react-icons/ai";
import { BiHide } from "react-icons/bi";

function Signup() {
  return (
    <>
      <div className="Signup">
        <img src={Background} alt="Background" className="Background" />
        <h4 className="logo">Your Logo</h4>
      </div>

      <div className="Sign-up">
      <div className="Sign">
        <div>
          <h2>Sign up</h2>
        </div>
        <div className="register">
          <p>If you already have an account register</p>
          <p>
            You can <span>Login here !</span>
          </p>
        </div>
        <br></br>
        <p>Email</p>
        <div className="Mail">
          
          <br></br>
          <div className="gomail">
            <GoMail />
            <input
              type="text"
              name="email"
              placeholder="Enter your email address"
              required="required"
            ></input>
          </div>
          <hr className="line"></hr>
        </div>
        <br></br>
        <p>Username</p>
        <div className="Username">
          
          <br></br>
          <div className="user">
            <AiOutlineUser />
            <input
              type="text"
              name="username"
              placeholder="Enter your User name"
              required="required"
            ></input>
          </div>
          <hr></hr>
        </div>
        <br></br>
        <p>Password</p>
        <div className="Password">
         
          <br></br>
          <div className="locks">
            <AiOutlineLock />
            <input
              type="text"
              name="Password"
              placeholder="Enter your Password"
              required="required"
            ></input>
            <BiHide/>
          </div>
          <hr></hr>
        </div>
        <br></br>
        <p>Confrim Password</p>
        <div className="Confrim">
          
          <br></br>
          <div className="lock">
            <AiOutlineLock />
            <input
              type="text"
              name="Confrim"
              placeholder="Confrim your Password"
              required="required"
            ></input>
            <BiHide/>
          </div>
          <hr></hr>
          <br></br>
        </div>
        <button onclick="myFunction(Register)"  type="submit" className="btn ">Register</button>
      </div>
      </div>
    </>
  );
}

export default Signup;
